from django.urls import path

from tasks.views import TaskDetail, TaskList, TaskDelete

urlpatterns = [
    path('', TaskList.as_view(), name='task-list'),
    path('<int:pk>/', TaskDetail.as_view(), name='task-detail'),
    path('<int:pk>/delete/', TaskDelete.as_view(), name='task-delete')
]