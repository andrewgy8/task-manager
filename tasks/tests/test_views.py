import pytest
from django.contrib.auth.models import User
from tasks.models import Task


@pytest.fixture
def user():
    return User.objects.create_user(email='hello@gmail.com',
                                    username='andrew',
                                    password='aPassword')


@pytest.fixture
def new_task(user):
    return Task.objects.create(title='Do something', user=user)


@pytest.mark.django_db
class TestViews:
    def test_returns_200_when_detail_view(self, new_task, client):
        url = f'/tasks/{new_task.id}/'

        res = client.get(url)

        assert res.status_code == 200
        assert res.template_name[0] == 'tasks/detail.html'

    def test_returns_200_when_list_view(self, new_task, client):
        url = f'/tasks/'

        res = client.get(url)

        assert res.status_code == 200
        assert res.template_name[0] == 'tasks/list.html'
        assert new_task.title in res.content.decode()

    def test_redirects_to_task_list_when_deleted(self, new_task, client):
        url = f'/tasks/{new_task.id}/delete/'

        res = client.get(url)

        assert res.status_code == 200
        assert res.template_name[0] == 'tasks/task_confirm_delete.html'
