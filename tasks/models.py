from django.contrib.auth.models import User
from django.db import models


PENDING = 0
COMPLETE = 1

STATUSES = (
    (PENDING, 'Pending'),
    (COMPLETE, 'Complete')
)


class Task(models.Model):
    title = models.CharField(max_length=200, blank=False, null=False)
    status = models.PositiveIntegerField(choices=STATUSES, default=PENDING)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
