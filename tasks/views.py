from django.views import generic
from django.urls import reverse_lazy

from .models import Task


class TaskDetail(generic.DetailView):
    model = Task
    template_name = 'tasks/detail.html'


class TaskList(generic.ListView):
    model = Task
    template_name = 'tasks/list.html'


class TaskDelete(generic.DeleteView):
    model = Task
    success_url = reverse_lazy('task-list')
