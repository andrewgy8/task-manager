from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from emails.email_client import login, decode, latest_email
from tasks.models import Task


class Command(BaseCommand):
    help = 'Saves email from client.'

    def handle(self, *args, **options):
        mail = login()
        raw = latest_email(mail)
        mail = decode(raw)

        from_mail = mail['From']
        subject = mail['Subject']
        if Task.objects.get(title=subject):
            self.stdout.write(self.style.SUCCESS(f'Task with this title already exists.'))
            return

        email = from_mail.split(' ')[2][1:-1]
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            name = from_mail.split(' ')
            user = User.objects.create_user(username=name[0], email=email, password='happy1234')

        task = Task.objects.create(title=subject, user=user)

        self.stdout.write(self.style.SUCCESS(f'Successfully created a task: {task.title}'))


