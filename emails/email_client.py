import imaplib
import email


def login():
    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login('gypemail8@gmail.com', 'Vespa888')
    mail.select("inbox")
    return mail


def latest_email(mail):
    _, data = mail.uid('search', None, "ALL")
    latest_email_uid = data[0].split()[-1]
    result, data = mail.uid('fetch', latest_email_uid, '(RFC822)')
    raw_email = data[0][1]
    return raw_email


def decode(raw_email):
    return email.message_from_string(raw_email.decode())
